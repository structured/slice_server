#!/usr/bin/env python

import time as timemod
import sys
import os
import intake
import xarray as xr 
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as crs
import haversine as hs
import yaml
import itertools


import cartopy.crs as ccrs
import datashader as ds
# import geoviews as gv
import holoviews as hv
import panel as pn
import pandas as pd
from datashader import reductions as rd, transfer_functions as tf
from holoviews.operation.datashader import datashade, rasterize, regrid
from holoviews import streams

from datetime import timedelta, datetime

import helpers as h
import grid_hierarchy as gh

# hv.extension("bokeh")

import aiohttp
import asyncio


async def on_request_start(session, trace_config_ctx, params):
    trace_config_ctx.start = asyncio.get_event_loop().time()
    print(params.method, params.url)

async def on_request_end(session, trace_config_ctx, params):
    elapsed = asyncio.get_event_loop().time() - trace_config_ctx.start
    print(f"{params.method} {params.url} done {params.response.status} {elapsed}")

trace_config = aiohttp.TraceConfig()
trace_config.on_request_start.append(on_request_start)
trace_config.on_request_end.append(on_request_end)

trace_configs = [trace_config]

opts = {"storage_options": {"client_kwargs": {"trace_configs": trace_configs}}}

cat_dict = {}
cat_dict['NextGEMS.MPIM-DWD-DKRZ.ICON-SAP-5km.Cycle1.atm.3hr.gn.ml.dpp0052.inst'] = xr.open_zarr('https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/nextGEMS/nextGEMS_ICON_3d_hus.zarr', consolidated=True, **opts)
cat_dict['NextGEMS.MPIM-DWD-DKRZ.ICON-SAP-5km.Cycle1.atm.30min.gn.2d.dpp0052.inst'] = xr.open_zarr('https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/nextGEMS/nextGEMS_ICON_3d_prw.zarr', consolidated=True, **opts)

data_dict = {}
data_dict[6] = xr.open_zarr('https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/nextGEMS/nextGEMS_ICON_3d_prw_R02B06.zarr', consolidated=True, **opts)
data_dict[7] = xr.open_zarr('https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/nextGEMS/nextGEMS_ICON_3d_prw_R02B07.zarr', consolidated=True, **opts)
data_dict[8] = xr.open_zarr('https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/nextGEMS/nextGEMS_ICON_3d_prw_R02B08.zarr', consolidated=True, **opts)
data_dict[9] = cat_dict['NextGEMS.MPIM-DWD-DKRZ.ICON-SAP-5km.Cycle1.atm.30min.gn.2d.dpp0052.inst']


gridcat = intake.open_catalog("https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/nextGEMS/grids/global_grids.yaml")
grid_dict = {gridcat[n].metadata["grid_level"]: gridcat[n].to_dask() for n in gridcat}
grids_until_B9 = [g for l, g in sorted(grid_dict.items()) if l <= 9]
grids = gh.GridHierarchy(grids_until_B9)


key = 'NextGEMS.MPIM-DWD-DKRZ.ICON-SAP-5km.Cycle1.atm.30min.gn.2d.dpp0052.inst'
data_w_coords = h.get_coords(cat_dict, key, 'prw')
data = data_w_coords[0]
lat = data_w_coords[1]
lon = data_w_coords[2]


key = 'NextGEMS.MPIM-DWD-DKRZ.ICON-SAP-5km.Cycle1.atm.3hr.gn.ml.dpp0052.inst'
data_w_coords = h.get_coords(cat_dict, key, 'hus')
data_3D = data_w_coords[0]
lat = data_w_coords[1]
lon = data_w_coords[2]


def is_clockwise(a1, a2, b1, b2, c1, c2):
    return ((c1-b1)*(c2-a2)-(c2-b2)*(c1-a1)) > 0

def make_trimesh(vertex_of_cells, vx, vy):
    n1, n2, n3 = vertex_of_cells
    # Test if clockwise
    cw = is_clockwise(vx[n1], vy[n1], vx[n2], vy[n2], vx[n3], vy[n3])
    n2_temp = n2
    n3_temp = n3
    # Make clockwise
    n2 = np.where(cw, n3_temp, n2_temp)
    n3 = np.where(cw, n2_temp, n3_temp)
    # Cells
    cdf = xr.Dataset({
        "node1": ("cell", n1),
        "node2": ("cell", n2),
        "node3": ("cell", n3),
    })
    # Vertices
    vdf = xr.Dataset({"x": ("vertex", vx), "y": ("vertex", vy)})
    return cdf, vdf


grid = grid_dict[9]


def select_data(time, var, x_range, y_range):
    """
    Select subset of data
    """
    start = timemod.time()
    print("start")
    size = min((x_range[1]-x_range[0])/180, (y_range[1]-y_range[0])/360)
    if size > 0.5:
        lev = 7
    elif size > 0.25:
        lev = 7
    elif size > 0.125:
        lev = 8
    else:
        lev = 9

    grid = grid_dict[lev]
    data = data_dict[lev]
    cell = ((grid.clat.values >= np.deg2rad(y_range[0])) &
            (grid.clat.values <= np.deg2rad(y_range[1])) &
            (grid.clon.values >= np.deg2rad(x_range[0])) &
            (grid.clon.values <= np.deg2rad(x_range[1])))
    t1 = timemod.time()
    print(f"have cells {t1 - start}")

    cells, verts = make_trimesh((grid.vertex_of_cell[:,cell]-1).values,
                            np.rad2deg(grid.vlon).values,
                            np.rad2deg(grid.vlat).values)
    t2 = timemod.time()
    print(f"have trimesh {t2 - t1}")
    selected_data = data[var].isel(time=time,ncells=cell).squeeze(drop=True)
    selected_data = (selected_data.rename({"ncells": "cell"}).drop(['time'])/4**(9-lev)).load()
    t3 = timemod.time()
    print(f"have selected {t3 - t2}")
    # Select specific cells and populate data
    celldata = cells.assign(variable=selected_data)  #.isel(cell=cell)
    t4 = timemod.time()
    print(f"before dataframe {t4-t3}")
    celldata_df = celldata.to_dataframe()
    verts_df = verts.to_dataframe()
    t5 = timemod.time()
    print(f"after dataframes {t5 -t4}")
    return celldata_df, verts_df, selected_data


data = data.assign_coords({'time':[h.convert_ymdf2dt(t) for t in data.time.data]})
json_obj = {}

def prepare_plot(var, x_range, y_range, value, **kwargs):
    global json_obj
    global json_pane
    celldata_df, verts_df, sel_data = select_data(value, var, x_range, y_range)
    ymd_hm = str(data.time.isel(time=value).dt.strftime('%Y/%m/%d %H:%M UTC').values)
    label = f"{key}\n{ymd_hm}"
    json_obj['time']= value
    json_obj['regional_sel'] = {'xrange': x_range, 'yrange': y_range}
    json_pane.param.trigger('object')
    return hv.TriMesh((celldata_df, verts_df), 
        label=label,
        crs=ccrs.PlateCarree())


def create_vectors(lat1,lat2,lon1,lon2):
    o = np.array([lon1, lat1])  # origin vector of line segment
    v = np.array([lon2 - lon1, lat2 - lat1])  # vector along the line segment
    n = np.array([v[1], -v[0]])  # vector normal to line segment
    n /= np.linalg.norm(n) # make normal vector a unit vector
    A = np.stack([v,n],axis=-1)  # stack to matrix
    return A, o, v

def create_points(lons, lats):
    pts = np.stack([lons, lats],axis=1)
    return pts

def get_distances(A, pts, origin):
    """
    s : np.array
      signed distances
    """
    l, s = np.linalg.inv(A)@(pts-origin).T
    return l, s

def create_idx(l, s, indices_lookup):
    """
    indices_lookup : np.array()
        indices of nodes, edges or cells, depending on l and s
    """
    
    idx = ( np.any(s[indices_lookup] >0, axis=0 )
      & np.any(s[indices_lookup] <0, axis=0 )
      & np.any(l[indices_lookup] >0, axis=0 )
      & np.any(l[indices_lookup] <1, axis=0 )
      )
    return idx

def sort_idx_by_lambda(idx, l):
    cl_idx_sorted = np.argsort(l)
    idx_sorted = np.where(idx)[0][cl_idx_sorted]
    return idx_sorted

def get_idx_along_line(lat1,lat2,lon1,lon2, grid):
    A, o, v = create_vectors(lat1,lat2,lon1,lon2)
    vlons_deg = np.rad2deg(grid.vlon)
    vlats_deg = np.rad2deg(grid.vlat)
    clons_deg = np.rad2deg(grid.clon)
    clats_deg = np.rad2deg(grid.clat)
    vpts = create_points(vlons_deg, vlats_deg)

    vl, vs = get_distances(A, vpts, o)
    vertex_of_cells = (grid.vertex_of_cell-1).values
    vidx = create_idx(vl, vs, vertex_of_cells)
    cpts = create_points(clons_deg[vidx], clats_deg[vidx])
    cl, cs = get_distances(A, cpts, o)

    # Get coords along line that are orthogonal to the center points
    pts=(cl * v[:, None]).T+o  #pts[:,0] --> longitudes; pts[:,1] -->latitudes
    
    vidx_sorted = sort_idx_by_lambda(vidx, cl)
    return vidx_sorted, pts[np.argsort(cl)]


def prepare_plot_crosssection(data=None, var_3D='hus', value=0, **kwargs):
    print('Streaming')
    if data is not None:
        if data['xs'] != []:
            lons = data['xs'][0]
            lats = data['ys'][0]
            xyz = gh.latlon2xyz(np.deg2rad(lats), np.deg2rad(lons))
            ind = grids.locate_cells_by_polyline_xyz(xyz, return_sorted=True)
            json_obj['crossection'] = {'cells': ind.tolist(), 'points_lat': lats, 'points_lon': lons}
            json_pane.param.trigger('object')
#             ind = []
#             cv_pts = []
#             for i in range(len(lons)-1):
# #                 idx, cv_pts_ = get_idx_along_line(lats[i],lats[i+1],lons[i],lons[i+1], grid)
#                 idx = gh.locate_cells_by_polyline_xyz()
#                 ind.extend(idx)
#                 cv_pts.append(cv_pts_)
#             cv_pts = np.vstack(cv_pts)

            # Calculate distances between projected center lats on direct line
#             pts = np.stack([cv_pts[:,1],cv_pts[:,0]],axis=1)
#             dist_km = hs.haversine_vector(pts[0:-1],pts[1:])
#             dist_km = np.hstack([[0],dist_km])
#             dist_km_from_o = np.cumsum(dist_km)
        else:
            ind = np.arange(0,100)
    else:
        ind = np.arange(0,100)

    ilo = ind.min()
    ihi = ind.max()

    #data_to_plot = data_3D[var_3D].isel(time=value, ncells=slice(ilo, ihi+1)).isel(ncells=ind-ilo).load()
    t8 = timemod.time()
    data_to_plot = data_3D[var_3D].isel(time=value, ncells=ind).load()
    t9 = timemod.time()
    print(f"data_to_plot load {t9 -t8}")
#     mean_pres = data_3d.pres.isel(time=0).mean(dim='ncells').compute()

    print(data_to_plot.encoding)
    d = (data_to_plot-data_to_plot.mean(dim='ncells'))
    d = d.squeeze().drop(['time'])
    d['ncells'] = d.ncells  # Should be dist_km_from_o
#         max_abs_val = np.max([d.max().compute(), np.abs(d.min().compute())])
#     plt.pcolormesh(dist_km_from_o, mean_pres/100., d,
#                    cmap='RdBu', shading='nearest',
#                    vmin=-max_abs_val, vmax=max_abs_val)

#         plt.ylabel('pressure / hPa')
#         plt.xlabel('distance / km')
#         plt.gca().invert_yaxis()
#         cb = plt.colorbar()
#         cb.set_label('$\Delta$ qv / gkg$^{-1}$')
    p=hv.Dataset(d).to(hv.Image, ['ncells', 'height'])
    return p


slider = pn.widgets.IntSlider(start=0, end=len(data.time), value=1, name='Time')
json_pane = pn.pane.JSON(json_obj, name='JSON', height=300, width=500)


# Define stream to only load subsection when zoomed in
path = hv.Path([])
stream = streams.RangeXY(x_range=(np.rad2deg(grid.clon.min().values),
                                  np.rad2deg(grid.clon.max().values)), 
                         y_range=(np.rad2deg(grid.clat.min().values),
                                  np.rad2deg(grid.clat.max().values)), linked=True)
path_stream = streams.PolyDraw(source=path, drag=True, show_vertices=True, num_objects=1)
dmap=hv.DynamicMap(prepare_plot, streams=[stream, slider], kdims=['var']).redim.values(var=list(data.data_vars))
dmap_crosssection=hv.DynamicMap(prepare_plot_crosssection, streams=[path_stream, slider], kdims=['var_3D']).redim.values(var_3D=list(data_3D.data_vars)).opts(norm=dict(framewise=True))

rastered=rasterize(dmap, aggregator=ds.mean('variable'))

hv_plot=(rastered).opts(
    colorbar=True, cmap='Blues', width=500, height=500, projection=ccrs.PlateCarree(), tools=['hover'],
#     clim=(0.6,0.95)
) * path

hv2_plot=(dmap_crosssection).opts(
    colorbar=True, cmap='Blues', width=500, height=300)

json_pane = pn.pane.JSON(json_obj, name='API request', height=300, width=500)

pn.Column(pn.Row((hv_plot + hv2_plot), slider),json_pane).servable()

