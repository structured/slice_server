# slice server

build the docker image:

```bash
docker build . -t slice_server
```

run the image:
```
docker run -it --rm -p5006:5006 slice_server
```

access the server: http://localhost:5006
