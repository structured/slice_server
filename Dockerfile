FROM python:3.9

WORKDIR /usr/src/app

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update \
 && apt install -y llvm libproj-dev libgeos-dev \
 && rm -rf /var/lib/apt/lists/*

COPY requirements.txt ./
RUN pip install --no-cache-dir --upgrade pip setuptools \
 && pip install --no-cache-dir Cython numpy \
 && pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5006/tcp
CMD [ "panel", "serve", "app.py" ]
