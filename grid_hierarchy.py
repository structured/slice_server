import numpy as np

def cells_to_child_cells(cells):
    return (4 * cells[...,np.newaxis] + np.arange(4)).ravel()

def grid_has_factor4_parent_index(grid):
    """
    returns true if grid indexing is appropriate for hierarchical access
    """
    cir = grid.parent_cell_index.values.reshape(-1,4)
    return np.all(cir[:,0,np.newaxis] == cir)

def latlon2xyz(lat, lon):
    """
    convert latitude and longitude coordinates to normalized xyz coordinates
    units must be radians
    """
    slat = np.sin(lat)
    slon = np.sin(lon)
    clat = np.cos(lat)
    clon = np.cos(lon)
    return np.stack([clat * clon, clat * slon, slat], axis=-1)

class GridHierarchy:
    cell_expansion_factor = 1
    
    def __init__(self, grids):
        """
        grids: list of icon grids as xarray dataset

        grid must be a full hierarchy, corases grid must be first
        """
        self.grids = grids
        self.verts = [
            np.stack([grid.cartesian_x_vertices.values,
                      grid.cartesian_y_vertices.values,
                      grid.cartesian_z_vertices.values], axis=-1)
            for grid in grids]
        self.vocs = [grid.vertex_of_cell.values - 1 for grid in grids]
  
    def locate_cells_by_point_xyz_at_level(self, p, level=0, subset=None):
        if subset is None:
            subslice = slice(None)
        else:
            subslice = subset
        
        v1, v2, v3 = self.verts[level][self.vocs[level][:, subslice]]
        n1 = np.cross(v2, v3)
        n2 = np.cross(v3, v1)
        n3 = np.cross(v1, v2)

        o1 = np.einsum("...i,ni->...n", p, n1)
        o2 = np.einsum("...i,ni->...n", p, n2)
        o3 = np.einsum("...i,ni->...n", p, n3)

        s = np.einsum("ni,ni->n", v1, n1) # check the ordering of the triangle

        valid = (s*o1 >= 0) & (s*o2 >= 0) & (s*o3 >= 0)
        idx = np.where(valid)[0]
        if subset is None:
            return idx
        else:
            return subset[idx]
    
    def locate_cells_by_point_xyz(self, p, subset=None):
        """
        based on a point in normalized xyz coordinates, find cell on finest grid containing this point
        """
        for level in range(len(self.grids)):
            cells = self.locate_cells_by_point_xyz_at_level(p, level, subset)
            subset = cells_to_child_cells(self.expand_cells(cells, level, self.cell_expansion_factor))
        return cells
    
    def locate_cells_by_polyline_xyz_at_level(self, polyline, level=0, return_sorted=False, subset=None):
        if subset is None:
            subslice = slice(None)
        else:
            subslice = subset
        
        plane_normals = np.cross(polyline[:-1,:], polyline[1:,:])
        plane_normals /= np.linalg.norm(plane_normals, axis=-1)[..., np.newaxis]
        plane_alongs = polyline[1:,:] - polyline[:-1,:]
        plane_ups = np.cross(plane_alongs, plane_normals)
        assert np.all(np.einsum("...i,...i->...", plane_ups, polyline[:-1,:]) > 0)
        
        A = np.stack([plane_alongs, plane_normals, plane_ups], axis=-1)
        Ainv = np.linalg.inv(A)
        l, s, u = np.einsum("pnv,kcv->nkpc", Ainv, self.verts[level][self.vocs[level][:, subslice]])
        # shape of lsu: vertex_of_cell, plane_index, cell_index
        
        on_both_sides_of_plane = np.any(s<0, axis=0) & np.any(s>0, axis=0)
        within_length_of_segment = np.any(l>-.5, axis=0) & np.any(l<.5, axis=0)
        on_this_hemisphere = np.any(u>0, axis=0)
        
        valid = on_both_sides_of_plane & \
                within_length_of_segment & \
                on_this_hemisphere
        
        if return_sorted:
            idx = []
            for seg_l, seg_valid in zip(np.mean(l, axis=0), valid):
                seg_idx = np.where(seg_valid)[0]
                seg_idx = seg_idx[np.argsort(seg_l[seg_idx])]
                if idx and idx[-1][-1] == seg_idx[0]:
                    idx.append(seg_idx[1:])
                else:
                    idx.append(seg_idx)
            idx = np.concatenate(idx)
        else:
            valid = np.any(valid, axis=0)  # check if any plane has a match
            idx = np.where(valid)[0]
        if subset is None:
            return idx
        else:
            return subset[idx]
        
    def locate_cells_by_polyline_xyz(self, polyline, return_sorted=False, subset=None):
        """
        based on a polyline in normalized xyz coordinates, find cells intersected by the line
        this method interpolated along great circles
        """
        for level in range(len(self.grids)):
            cells = self.locate_cells_by_polyline_xyz_at_level(polyline,
                                                               level,
                                                               return_sorted=return_sorted and level==len(self.grids)-1,
                                                               subset=subset)
            subset = cells_to_child_cells(self.expand_cells(cells, level, self.cell_expansion_factor))
        return cells
    
    def expand_cells(self, cells, level, iterations=1):
        for i in range(iterations):
            cells = np.unique(np.concatenate([cells, self.grids[level].neighbor_cell_index.values[:,cells].ravel()-1]))
        return cells


