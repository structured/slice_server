import os
from pathlib import Path
import numpy as np
import xarray as xr
from datetime import datetime, timedelta


def name2dict(name):
    (project, institute, model, experiment, domain, frequency, grid, level_type, ensemble_member, operation) = name.split('.')  
    ret =   {"project" : project, "institute" : institute, "model" : model, "experiment" :experiment, "domain" : domain, "frequency" : frequency, "grid": grid, "level_type" : level_type, "ensemble_member" : ensemble_member, "operation" : operation}
    print (ret)
    return ret


def guess_grid_file(name):
    nd = name2dict (name) # /work/ka1081/DYAMOND_WINTER/MPIM-DWD-DKRZ/ICON-SAP-5km/DW-CPL/atmos/fx/gn/
    path_option_1 = "/work/ka1081/{project}/{institute}/{model}/{experiment}/{domain}/fx/{grid}/grid.nc".format(**nd)
    path_option_2 = "/scratch/local1/m300408/nextGEMS/grids/grid.nc"
    if os.path.exists(path_option_1):
        return path_option_1
    else:
        return path_option_2


def get_coords(cat_dict, x, var):
    root = '/work/ka1081/'
    end = "/grid.nc"
    lat = None
    lon = None
    grid = None
    print ("Processing " + x)
    components = x.split(".")
    image_filename = "tas_" + x + ".png"
    try:
        grid_file_name = os.path.realpath(guess_grid_file(x))
        grid = xr.open_dataset (grid_file_name)
        ds = xr.merge([cat_dict[x], grid])
        print ("loaded grid from file " + grid_file_name)
    except:
        print ("could not open a grid file at " + grid_file_name)
        ds = cat_dict[x] 
    lat, lon = guess_coords(ds, var)
    print ('got coordinates')
    if lat.max()> 4:
        print ("converting degree to radian")
        lat = lat*np.pi/180.
        lon = lon*np.pi/180.
        lat['units'] = 'radian'
        lon['units'] = 'radian'

    if (lon.max() > np.pi):
        print ("shifting from 0 to 2pi to -pi to pi")
        lon = lon -  ( 2 * np.pi * (lon > np.pi))
#    print ("max lon ", lon.max())
    return (ds, lat, lon)


def guess_coords(ds, var):
    try:
#        print (ds[var].coordinates)
        lon, lat = [ds[x] for x in ds[var].coordinates.split()]
        print ("using " + ds[var].coordinates + " from coordinates attribute as coordinates.")
    except:
        lat = None
        lon = None
        try:
            lon = ds.clon
            lat = ds.clat
            print ("using clat and clon directly.")
        except:
            try:
                lon = ds.lon
                lat = ds.lat
            except:
                lat = None
                lon = None
                try:
                    for co in ds[var].coords.keys():
                        print ("trying variables from coords property " + co)
                        if "lon" in co:
                            print ("found " + co + " for *lon*.")
                            lon = ds[co]
                        if "lat" in co:
                            print ("found " + co + " for *lat*.")
                            lat = ds[co]
                    lat.shape
                except:
                    lat = None
                    lon = None

                    lon = ds[ds[var].dims[-1]]
                    lat = ds[ds[var].dims[-2]]
                    print ("using " + ds[var].dims[-1] + " as lon")
                    print ("using " + ds[var].dims[-2] + " as lat")
    return (lat, lon)


def convert_ymdf2dt(ymdf):
    ymd = str(np.int(np.floor(ymdf)))
    day_frac = ymdf % 1
    return datetime.strptime(str(ymd), "%Y%m%d")+timedelta(days=day_frac)


def convert_ICONtime(dataset):
    dataset = dataset.assign_coords({'time':[convert_ymdf2dt(t) for t in dataset.time.data]})
    return dataset


def select_by_latlon(grid, lats, lons):
    """
    Select subset of data
    """
    cell = ((grid.clat >= np.deg2rad(lats[0])) &
            (grid.clat <= np.deg2rad(lats[1])) &
            (grid.clon >= np.deg2rad(lons[0])) &
            (grid.clon <= np.deg2rad(lons[1]))).values
    return cell